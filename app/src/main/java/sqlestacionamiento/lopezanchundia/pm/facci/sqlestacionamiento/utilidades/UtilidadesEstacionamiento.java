package sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento.utilidades;

public class UtilidadesEstacionamiento {
    public static final String TABLA_ESTACIONAMIENTO = "estacionamientos";
    public static  final String CAMPO_ID ="id";
    public static  final String CAMPO_PLACA ="placa";
    public static  final String CAMPO_FECHA ="fecha";
    public static  final String CAMPO_HORA ="hora";
    public  static  final String CAMPO_COLOR = "color";


    public static  final String CREAR_TABLA_ESTACIONAMIENTO ="CREATE TABLE "+
            TABLA_ESTACIONAMIENTO+" ("+
            CAMPO_ID+" INTEGER, "+
            CAMPO_PLACA+" TEXT, "+
            CAMPO_FECHA+" TEXT, "+
            CAMPO_HORA+" TEXT, "+
            CAMPO_COLOR+" TEXT)";
}
