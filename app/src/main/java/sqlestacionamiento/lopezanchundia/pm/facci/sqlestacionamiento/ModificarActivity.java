package sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento.utilidades.UtilidadesEstacionamiento;

public class ModificarActivity extends AppCompatActivity {
    EditText editTextColorModificar,editTextCodigoModificar;
    Button buttonModificar;
    ConectarSQLiteHelper conectar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);
        conectar = new ConectarSQLiteHelper(this,"bd estacionamientos",null,1);
        editTextCodigoModificar=(EditText)findViewById(R.id.editTextCodigoModificar);
        editTextColorModificar=(EditText)findViewById(R.id.editTextColorModificar);
        buttonModificar=(Button)findViewById(R.id.buttonModificar);

        buttonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModificarColor();
            }
        });
    }

    private void ModificarColor() {
        SQLiteDatabase db = conectar.getWritableDatabase();
        String[] parametro = {editTextCodigoModificar.getText().toString()};
        ContentValues values = new ContentValues();
        values.put(UtilidadesEstacionamiento.CAMPO_COLOR,editTextColorModificar.getText().toString());
        db.update(UtilidadesEstacionamiento.TABLA_ESTACIONAMIENTO,values,UtilidadesEstacionamiento.CAMPO_ID+"=?",parametro);
        Toast.makeText(getApplicationContext(),"El color fue modificado",Toast.LENGTH_LONG).show();
        db.close();
    }
}
