package sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento.utilidades.UtilidadesEstacionamiento;

public class RegistrarEstacionamientoActivity extends AppCompatActivity {
    ConectarSQLiteHelper conectar;
    EditText editTextIdRegistrar,editTextFecha,editTextPlaca,editTextHora,editTextColor;
    Button buttonRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_estacionamiento);
        conectar = new ConectarSQLiteHelper(this,"bd estacionamientos",null,1);
        editTextFecha = (EditText)findViewById(R.id.editTextFecha);
        editTextIdRegistrar = (EditText)findViewById(R.id.editTextIdRegistro);
        editTextHora=(EditText)findViewById(R.id.editTextHora);
        editTextPlaca=(EditText)findViewById(R.id.editTextPlaca);
        editTextColor=(EditText)findViewById(R.id.editTextColor);
        buttonRegistrar=(Button)findViewById(R.id.buttonRegistrar);


        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarEstacionamiento();
            }
        });
    }

    private void registrarEstacionamiento() {
        SQLiteDatabase db = conectar.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UtilidadesEstacionamiento.CAMPO_ID,editTextIdRegistrar.getText().toString());
        values.put(UtilidadesEstacionamiento.CAMPO_PLACA,editTextPlaca.getText().toString());
        values.put(UtilidadesEstacionamiento.CAMPO_HORA,editTextHora.getText().toString());
        values.put(UtilidadesEstacionamiento.CAMPO_FECHA,editTextFecha.getText().toString());
        values.put(UtilidadesEstacionamiento.CAMPO_COLOR,editTextColor.getText().toString());

        Long idResultatnte= db.insert(UtilidadesEstacionamiento.TABLA_ESTACIONAMIENTO,UtilidadesEstacionamiento.CAMPO_ID,values);
        Toast.makeText(getApplicationContext(),"Registro de estacionamiento: "+idResultatnte,Toast.LENGTH_LONG).show();
        db.close();

    }
}
