package sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento.utilidades.UtilidadesEstacionamiento;

public class ConectarSQLiteHelper extends SQLiteOpenHelper {

    public ConectarSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UtilidadesEstacionamiento.CREAR_TABLA_ESTACIONAMIENTO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS estacionamientos");
        onCreate(db);

    }
}
