package sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento.entidad;

public class UsuarioEstacionamiento {
    private Integer id;
    private String placa;
    private String fecha;
    private String hora;
    private String color;

    public UsuarioEstacionamiento(Integer id, String placa, String fecha, String hora, String color) {
        this.id = id;
        this.placa = placa;
        this.fecha = fecha;
        this.hora = hora;
        this.color = color;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
