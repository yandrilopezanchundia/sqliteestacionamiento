package sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sqlestacionamiento.lopezanchundia.pm.facci.sqlestacionamiento.utilidades.UtilidadesEstacionamiento;

public class MainActivity extends AppCompatActivity {
    ConectarSQLiteHelper conectar;
    EditText editTextCodigo;
    Button buttonCodigo,buttonLllevarModificar;
    TextView textViewColor,textViewPlaca,textViewCodigo,textViewFecha,textViewHora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conectar = new ConectarSQLiteHelper(this,"bd estacionamientos",null,1);

        textViewCodigo=(TextView)findViewById(R.id.textViewCodigo);
        textViewFecha=(TextView)findViewById(R.id.textViewFecha);
        textViewHora=(TextView)findViewById(R.id.textViewHora);
        textViewPlaca=(TextView)findViewById(R.id.textViewPlaca);
        textViewColor=(TextView)findViewById(R.id.textViewColor);

        editTextCodigo=(EditText)findViewById(R.id.editTextCodigo);
        buttonCodigo=(Button)findViewById(R.id.buttonCodigo);
        buttonLllevarModificar=(Button)findViewById(R.id.buttonLlevarModificar);
        buttonLllevarModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ModificarActivity.class);
                startActivity(intent);
            }
        });




        buttonCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultar();

            }
        });



    }

    private void consultar() {
        SQLiteDatabase db = conectar.getReadableDatabase();
        String[] parametro = {editTextCodigo.getText().toString()};
        String[] campo = {UtilidadesEstacionamiento.CAMPO_ID,UtilidadesEstacionamiento.CAMPO_PLACA,UtilidadesEstacionamiento.CAMPO_FECHA,
                UtilidadesEstacionamiento.CAMPO_HORA,UtilidadesEstacionamiento.CAMPO_COLOR};

        try{
            Cursor cursor = db.query(UtilidadesEstacionamiento.TABLA_ESTACIONAMIENTO,campo,UtilidadesEstacionamiento.CAMPO_ID+"=?",parametro,null,null,null);
            cursor.moveToFirst();
            textViewCodigo.setText(cursor.getString(0));
            textViewPlaca.setText(cursor.getString(1));
            textViewFecha.setText(cursor.getString(2));
            textViewHora.setText(cursor.getString(3));
            textViewColor.setText(cursor.getString(4));
            cursor.close();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"El id no existe",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(MainActivity.this,RegistrarEstacionamientoActivity.class);
            startActivity(intent);


        }
        db.close();


    }


}
